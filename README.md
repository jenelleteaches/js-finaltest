# The Movies App

## Demo:

![](screenshots/demo.gif)

## Video Explanation:

[https://vimeo.com/329274147](https://vimeo.com/329274147)

Password: 0000


## How to Use

1. Download or clone the repository
2. Go to `starter_code` folder
3. Open `index.html` in your `browser`
4. In your browser, open `Developer Tools` and look at output
5. Modify the html/css/javascript as necessary


Mapping of HTML file with associated Javascript files:

* index.html --> scripts.js
* next.html --> next.js

CSS for both pages is controlled by the `styles.css` file

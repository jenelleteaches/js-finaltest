
// API URL for movie trailers is as follows

// Note 1: The only difference between the links is the nonsense written
// after the api_key parameter

// Note 2:  299537 = the MovieDB id of "Captain Marvel"

// ------
// First name: A-J, use:
// https://api.themoviedb.org/3/movie/299537/videos?api_key=041b7ff5799fd5e107ddb2e5d1211552&language=en-US

// First name: K-N, use:
// https://api.themoviedb.org/3/movie/299537/videos?api_key=6bcbe8d373dde04d0c37f09b69456976&language=en-US

// First name: O-Z, use:
// https://api.themoviedb.org/3/movie/299537/videos?api_key=b7e727c1562791e5c6dee2b45dd94afb&language=en-US
// ------

// @TODO: insert url for movie trailers
// Note: those `` symbols are there on purpose and not a mistake
// If you want to change the `` symobls to "", you absolutely can --
// but i suspect your life will be easier if you use string interpolation
// to generate the url
const URL = `https://jsonplaceholder.typicode.com/todos/1`

console.log("connecting to: " + URL);

var xhr = new XMLHttpRequest
xhr.open('GET', URL);
xhr.send(null);

xhr.onreadystatechange = function () {
    console.log("coming back!");
    const DONE = 4; // readyState 4 means the request is done.
    const OK = 200; // status 200 is a successful return.
    if (xhr.readyState === DONE) {
      if (xhr.status === OK) {

        // Get response from website and automatically
        // convert it to DICTIONARY or ARRAY
        let response = JSON.parse(xhr.responseText);
        console.log("Response from server: ")
        console.log(response)


        // get the video key from the API
        let videoId = "rTlAmiLKX_U"

        // create a youtube link
        let url = `https://www.youtube.com/embed/${videoId}?autoplay=1`

        // embed this into the website
        let html = `<iframe width="560" height="315" src="${url}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`
        document.getElementById("results").innerHTML = html

      } else {
        console.log('Error: ' + xhr.status); // An error occurred during the request.
      }
    }
}

function search()  {
    // clear the results <div>
    document.getElementById("results").innerHTML = ""

    // SAMPLE ENDPOINTS FOR NOW PlAYING
    // ---------------------------------

    // To reduce the load on MovieDB server, my suggestion is to use these APIS
    // Pick the API that matches your first name
    // The only difference between the links is the jumble of nonsense next to api_key

    // First name A-J, use:
    // https://api.themoviedb.org/3/movie/now_playing?api_key=041b7ff5799fd5e107ddb2e5d1211552&language=en-US&page=1

    // First name: K-N, use:
    // https://api.themoviedb.org/3/movie/now_playing?api_key=6bcbe8d373dde04d0c37f09b69456976&language=en-US&page=1

    // First name: O-Z, use:
    // https://api.themoviedb.org/3/movie/now_playing?api_key=b7e727c1562791e5c6dee2b45dd94afb&language=en-US&page=1


    // @TODO: add your URL here
    const URL = "https://jsonplaceholder.typicode.com/todos/1"
    console.log("connecting to: " + URL);

    var xhr = new XMLHttpRequest
    xhr.open('GET', URL);
    xhr.send(null);

    xhr.onreadystatechange = function () {
        console.log("coming back!");
        const DONE = 4; // readyState 4 means the request is done.
        const OK = 200; // status 200 is a successful return.
        if (xhr.readyState === DONE) {
          if (xhr.status === OK) {

            // Get response from website and automatically
            // convert it to DICTIONARY or ARRAY
            let response = JSON.parse(xhr.responseText);
            console.log("Response from server: ")
            console.log(response)


            // @TODO PUT YOUR CODE HERE
            // ----------------------------------------------



            // sample code - feel free to use or ignore
            let imageFile = "placeholder.gif"

            for (let i = 0; i < 3; i++) {

              let html = "<div>"
                html += "<h2> Pen Pineapple Apple Pen </h2>"
                html += `<img src=${imageFile}>`
                html += "<p> An extraordinary adventure in fruits and writing devices</p>"
              html += "</div>"

              document.getElementById("results").innerHTML += html
            }

            // ----------------------------------------------

          } else {
            console.log('Error: ' + xhr.status); // An error occurred during the request.
          }
        }
    }

}

function addToFavorites(name) {
  // debug: output whatever person clicked on
  console.log(name);
  alert("You clicked on " + name);

  //@TODO: Write your code here
  // ----------------------------------------------




  // ----------------------------------------------
}

function showFavorites() {
  // clear the results <div>
  document.getElementById("results").innerHTML = ""

  console.log("show favorites list pushed")
  alert("You pushed the showFavorites button!")

  document.getElementById("results").innerHTML = "No movies added!"

  //@TODO: Write your code here
  // ----------------------------------------------



  // ----------------------------------------------
}

// id = the id of the movie (from movie db)
function showTrailer(id) {
  //@TODO: Write your code here
  // ----------------------------------------------




  // ----------------------------------------------
}
